<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tech shop</title>
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <!-- Main -->
    <div class="main_page">
        <!-- Top Header -->
        <header class="header">
            <div class="top_header">
                <div class="top_header_container">
                    <div class="header_container_left_side">
                        <div class="language_select">
                            <select name="languages" id="languages">
                                <option value="ua" id="option">Українська</option>
                                <option value="eng">English</option>
                            </select>
                        </div>
                        <div class="currency_select">
                            <select name="currency" id="currency">
                                <option value="ua">UAH</option>
                                <option value="eng">USD</option>
                                <option value="eng">EUR</option>
                            </select>
                        </div>
                        <div class="phones_header">
                            <p>
                                <span class="callback_round">
                                    <img src="./icons/phone.ico" alt="telephone-icon">
                                </span>
                                <a href="tel:+380979999999">+38 097 999-99-99</a>
                            </p>
                        </div>
                        <a href="#" class="buy_on_click_header">
                            <span class="callback_round">
                                <img src="./icons/callback.ico" alt="callback-icon">
                            </span>
                            Передзвоніть
                        </a>
                        <a href="#" class="buy_on_click_header">
                            <span class="callback_round">
                                <img src="./icons/chat.ico" alt="chat-icon">
                            </span>
                            Чат
                        </a>
                    </div>
                    <div class="header_container_right_side">
                        <nav class="main_menu">
                            <ul>
                                <li class="menu_item">
                                    <a href="#">Головна</a>
                                </li>
                                <li class="menu_item">
                                    <a href="#">Доставка і оплата</a>
                                </li>
                                <li class="menu_item">
                                    <a href="#">Гарантія</a>
                                </li>
                                <li class="menu_item">
                                    <a href="#">Контакти</a>
                                </li>
                                <li>
                                    <a href="#">Увійти</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="middle_header">
                <div class="middle_header_container">
                    <div class="midhead_container_row">
                        <div class="logo">
                            <a href="#">
                                <img src="./icons/logo.png" alt="">
                            </a>
                        </div>
                        <div class="search_site">
                            <div class="main_search_form">
                                <form action="#" name="quick_find" class="form_search_site" method="get">
                                    <input type="search" id="search" class="search_site_input" placeholder="Пошук"
                                        name="keywords" value="" autocomplete="off">
                                    <button type="submit" id="search_form_button" class="search_site_submit">
                                        <img src="./icons/search.ico" alt="search-icon">
                                    </button>
                                </form>
                            </div>
                            <div class="divShoppingCard">
                                <div class="shopping_card">
                                    <span class="summ_basket">Кошик порожній</span>
                                    <img src="./icons/shopping-basket.ico" alt="shopping-basket">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar">
                <div class="navbar_container">
                    <div class="block_categories">
                        <ul class="nav_list">
                            <li><a href="#">Ігри</a></li>
                            <li><a href="#">Електротранспорт</a></li>
                            <li><a href="#">Побутова техніка</a></li>
                            <li><a href="#">Смартфони</a></li>
                            <li><a href="#">Планшети</a></li>
                            <li><a href="#">Ноутбуки</a></li>
                            <li><a href="#">Телевізори</a></li>
                            <li><a href="#">Аксесуари</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <!-- Main content -->
        <main class="main_content">
            <div class="main_container">
                <!-- center content  -->
            <div class="right_content">
                
            </div>
                <!-- left side bar -->
                <div class="left_side_bar">
                    <aside>
                        <div class="category_box">
                            <ul class="category_accordion">
                                <li class=cat_id1>
                                    <a href="#">Ігри</a>
                                </li>
                                <li class=cat_id2>
                                    <a href="#">Електротранспорт</a>
                                </li>
                                <li class=cat_id3>
                                    <a href="#">Побутова техніка</a>
                                </li>
                                <li class=cat_id3>
                                    <a href="#">Смартфони</a>
                                </li>
                                <li class=cat_id3>
                                    <a href="#">Планшети</a>
                                </li>
                                <li class=cat_id4>
                                    <a href="#">Ноутбуки</a>
                                </li>
                                <li class=cat_id5>
                                    <a href="#">Телевізори</a>
                                </li>
                                <li class=cat_id6>
                                    <a href="#">Аксесуари</a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </main>
        <!-- Footer -->
        <footer></footer>
    </div>

</body>

</html>